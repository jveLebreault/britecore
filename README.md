# BriteCore

BriteCore application mono repo, hosts the back-end python project and Vue.js front-end for the application app.

#### Problem analysis
The problem at hand can be summarized as trying to implement a way to give the users of the system the possibility to create custom risk types, each of those risk types has an arbitrary amount of fields that define the nature of the risk type.

Traditionally, relational databases are not well suited to handle data with arbitrary (unstructured) data, the traditional approach to model unstructured data in a relational database is to implement an [EAV](https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model) structure, also called vertical table(s). One of the ways to implement that solution would be to have a main table (in this case called RiskTypes) where the data related to the risk types will be saved and a second table (RiskType_fields) where all the fields will be stored and a Foreign key would point to the risk type that field is related to.  
With that setup you would end with a row for each field belonging to a particular risk type (hence the name of vertical table, the table grows vertically when adding new fields).  

In our particular scenario that approach would be troublesome to implement and work with because of the enum fields which would lead to a recursive reference to the RiskType_fields table or a third table to handle the enum fields separately.


#### My approach
After some thought it dawned on me that trying to handle unstructured data in a relational way is more trouble than what i thought, so i came to the conclusion that the best solution would be to use a format that is naturally unstructured and it hit me, JSON would be the solution to this problem.  
Using JSON any data can be represented no matter how arbitrary the shape of the data and the main DBMS's ([MySQL][mysql-json], [MS-SQL][ms-sql-json], [Postgres][postgres-json]) have capabilities to store and query validated JSON data.  
My mind was made, i was going to use JSON to model the fields (i think criteria sounds better to refer to those fields) and i ended with a table like this.

#### Entity-Relationship Diagram
![Entity-Relationship Diagram](./risktypes-app/src/assets/ERDiagram.png)

#### Deployment process
##### Front end 
The front end was built with VueJS and [Vue CLI][vue-cli], the deployment is made to an S3 bucket with a plugin for Vue CLI called [s3-deploy][vue-s3-deploy]. To run the deployment the following commands must be used:

* npm install
* npm run build
* npm run deploy
 

##### Back end
The service layer of the application was built with django rest framework and is deployed to an AWS lambda function using [Zappa][python-zappa], the lambda function is also connected to a MariaDB instance to handle the persistence of the risktypes.  To update the deployed function just run:

* pip install -r requirements.txt
* zappa update production


Note: To run the deployments a set of AWS credentials must be configured in the host computer.



##### Automated deploy
There is an automated build and deploy process that is triggered from a bitbucket pipeline for each of the sub-projects.


#### Links
[RiskTypes app][risktypes-app] front-end application  
[DRF RiskTypes api][risktypes-api] DRF endpoint


[vue-cli]:https://cli.vuejs.org/
[vue-s3-deploy]:https://github.com/multiplegeorges/vue-cli-plugin-s3-deploy
[python-zappa]:https://github.com/Miserlou/Zappa

[risktypes-api]: https://x1ksn3foba.execute-api.us-east-1.amazonaws.com/dev/risktypes

[risktypes-app]: http://risktypes-app.s3-website-us-east-1.amazonaws.com/

[ms-sql-json]: https://docs.microsoft.com/en-us/sql/relational-databases/json/json-data-sql-server?view=sql-server-2017 "MS SQL support for JSON"

[mysql-json]: https://dev.mysql.com/doc/refman/8.0/en/json.html "mySQL support for JSON"

[postgres-json]: https://www.postgresql.org/docs/9.4/datatype-json.html "postgrest support for JSON"