import json
from django.urls import reverse
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import RiskType
from .serializers import RiskTypeSerializer


class ModelTestCase(TestCase):
    """Test suite for the models of the app"""

    def setUp(self):
        """Creates a test RiskType for testing"""

        testJson = {'name': 'testCriteria', 'value': 'criteriaValue'}
        self.riskType = RiskType(name='Tests risk', description=testJson)

    def test_riskType_creation(self):
        criteriaCount = RiskType.objects.count()
        self.riskType.save()
        newCount = RiskType.objects.count()

        self.assertNotEqual(criteriaCount, newCount)


class ViewTestCase(TestCase):
    """Test suite for the api views"""

    def setUp(self):
        self.client = APIClient()
        self.riskTypeData = {
            'name': 'test risk',
            'description': 'my test risk',
            'criteria': json.dumps({
                'name': 'test name',
                'value': 'test value'
            })
        }
        self.response = self.client.post(
            reverse('create'),
            self.riskTypeData,
            format="json")

    def test_api_create_riskType(self):
        """Test the creation of a new riskType"""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_fetch_riskType(self):
        """Test get method for riskType api"""

        riskType = RiskType.objects.get()

        response = self.client.get(
            reverse('details', kwargs={'pk': riskType.id}),
            format='json'
        )

        serializer = RiskTypeSerializer(data=response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(serializer.is_valid())

    def test_api_update_riskType(self):
        """Tests api deletion for riskType api"""

        riskType = RiskType.objects.get()
        riskTypeChange = {
            'name': 'changed riskType',
            'description': 'riskType value',
            'criteria': json.dumps({'name': 'criteria name change'})
        }

        res = self.client.put(
            reverse('details', kwargs={'pk': riskType.id}),
            riskTypeChange,
            format='json',
        )

        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_delete_riskType(self):
        """Tests deletion of riskType for the api"""

        riskType = RiskType.objects.get()
        response = self.client.delete(
            reverse('details', kwargs={'pk': riskType.id}),
            format='json',
            follow=True,
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
