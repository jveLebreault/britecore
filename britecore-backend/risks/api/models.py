from django.db import models
from jsonfield import JSONField


# Create your models here.
class RiskType (models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=150)
    criteria = JSONField()
