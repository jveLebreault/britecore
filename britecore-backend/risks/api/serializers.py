from rest_framework import serializers
from .models import RiskType


class RiskTypeSerializer (serializers.ModelSerializer):

    criteria = serializers.JSONField()

    class Meta:
        model = RiskType
        fields = ('id', 'name', 'description', 'criteria')
