from rest_framework import generics
from .serializers import RiskTypeSerializer
from .models import RiskType


class RiskTypeListView(generics.ListCreateAPIView):
    """View that handles Creation and listing (POST and GET) of RiskTypes"""

    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer


class RiskTypeDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """View that handles retrieving, updating and deleting (GET, PUT, DELETE) of RiskTypes"""
    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer
