from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import RiskTypeListView, RiskTypeDetailsView


urlpatterns = {
    url(r'^risktypes/$', RiskTypeListView.as_view(), name="create"),
    url(r'^risktypes/(?P<pk>[0-9]+)/$', RiskTypeDetailsView.as_view(), name="details")
}

urlpatterns = format_suffix_patterns(urlpatterns)
