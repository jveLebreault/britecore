export default {
    props: {
        criterion: {
            type: Object,
            required: true
        },

        isEditable: {
            type: Boolean,
            required: false,
            default: true
        }
    },

    data() {
        return {
            dataCriterion: this.criterion
        };
    },

    methods: {
        removeCriterion() {
            this.$emit('remove-criterion');
        }
    }
};